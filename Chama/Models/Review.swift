//
//  Review.swift
//  Chama
//
//  Created by swan on 8.05.2020.
//  Copyright © 2020 csemresari. All rights reserved.
//

struct Review {
    
    var authorName: String
    var rating: Double
    var text: String
    var relativeTimeDescription: String
}
