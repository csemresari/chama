//
//  Place.swift
//  Chama
//
//  Created by swan on 8.05.2020.
//  Copyright © 2020 csemresari. All rights reserved.
//

struct Place {
    var placeId: String
    var name: String
    var rating: Double
    var types: [PlaceType]
    var openNow: Bool
    var location: Location
    var reviews: [Review]?
    var imageUrlString: String?
}


enum PlaceType: String {
    
    case restaurant
    case bar
    case cafe
    case other
    
    func toString() -> String {
        return self.rawValue.capitalized
    }
}

struct Location {
    var latitude: Double
    var longitude: Double
}
