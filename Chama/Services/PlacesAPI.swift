//
//  PlacesAPI.swift
//  Chama
//
//  Created by swan on 9.05.2020.
//  Copyright © 2020 csemresari. All rights reserved.
//

import Foundation
import SwiftyJSON

enum ErrorMessage: String {
    case Places = "Could not fecth places."
    case PlaceDetails = "Could not fetch place details."
    func Description() -> String {
        return self.rawValue
    }
}

let baseUrl = "https://maps.googleapis.com/maps/api/place/"
let GooglePlacesAPIKey = "AIzaSyA2z1uduA8gnERfJjWW1zbboAt_ZhN2wVQ"

class PlacesAPI: PlacesStoreProtocol {
    
    // MARK: Fetch Places
    
    func fetchPlaces(placeType: PlaceType, completionHandler: @escaping ([Place], PlacesStoreError?) -> Void) {

        guard let currentLocation = currentLocation else {
            print("Error: Cannot retrieve user location")
            return completionHandler([], .CannotFetch(ErrorMessage.Places.Description()))
        }
        
        let lat = currentLocation.coordinate.latitude
        let lng = currentLocation.coordinate.longitude
        
        let type = placeType.toString().lowercased()
        
        let urlString = baseUrl + "nearbysearch/json?location=\(lat),\(lng)&radius=50000&type=\(type)&rankby=prominence&key=\(GooglePlacesAPIKey)&fields=rating,opening_hours"
        
        guard let url = URL(string: urlString) else {
            print("Error: Cannot create URL from string")
            return completionHandler([], .CannotFetch(ErrorMessage.Places.Description()))
        }
        
        let urlRequest = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, _, error) in
            
            guard error == nil else {
                print("Error: Failure on API call")
                return completionHandler([], .CannotFetch(error!.localizedDescription))
            }
            
            guard let responseData = data else {
                print("Error: Empty JSON data")
                return completionHandler([], .CannotFetch(ErrorMessage.Places.Description()))
            }

            do {
                let json = try JSON(data: responseData)
                print(json)
                let results = json["results"].arrayValue
                var places: [Place] = []
                
                for item in results {
                    
                    let placeId = item["place_id"].stringValue
                    let name = item["name"].stringValue
                    let rating = item["rating"].doubleValue
                    let types = item["types"].arrayValue.map{ PlaceType(rawValue: $0.stringValue) ?? .other}
                    let openNow = item["opening_hours"]["open_now"].boolValue
                    let locationJson = item["geometry"]["location"]
                    let lat = locationJson["lat"].doubleValue
                    let lng = locationJson["lng"].doubleValue
                    let location = Location(latitude: lat, longitude: lng)
                    
                    let place = Place(placeId: placeId, name: name, rating: rating, types: types, openNow: openNow, location: location)
                    places.append(place)
                }
                return completionHandler(places, nil)
                
            } catch {
                print("Couldn't parse JSON")
                return completionHandler([], .CannotFetch(ErrorMessage.Places.Description()))
            }
        }
        task.resume()
        
    }
    
    func fetchPlaces(placeType: PlaceType, completionHandler: @escaping PlacesStoreFetchPlacesCompletionHandler) {}
    
    func fetchPlaces(placeType: PlaceType, completionHandler: @escaping (() throws -> [Place]) -> Void) {}
    
    
    // MARK: Fetch Place Details
    
    func fetchPlace(placeId: String, completionHandler: @escaping (Place?, PlacesStoreError?) -> Void){
        
        let urlString = baseUrl + "details/json?place_id=\(placeId)&fields=name,rating,opening_hours,review,photo&key=\(GooglePlacesAPIKey)"
        
        guard let url = URL(string: urlString) else {
            print("Error: Cannot create URL from string")
            return completionHandler(nil, .CannotFetch(ErrorMessage.PlaceDetails.Description()))
        }
        
        let urlRequest = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, _, error) in
            
            guard error == nil else {
                print("Error: Failure on API call")
                return completionHandler(nil, .CannotFetch(error!.localizedDescription))
            }
            
            guard let responseData = data else {
                print("Error: Empty JSON data")
                return completionHandler(nil, .CannotFetch(ErrorMessage.PlaceDetails.Description()))
            }
            
            do {
                let json = try JSON(data: responseData)
                let item = json["result"]
                
                let placeId = item["place_id"].stringValue
                let name = item["name"].stringValue
                let rating = item["rating"].doubleValue
                let types = item["types"].arrayValue.map{ PlaceType(rawValue: $0.stringValue) ?? .other}
                let openNow = item["opening_hours"]["open_now"].boolValue
                let locationJson = item["geometry"]["location"]
                let lat = locationJson["lat"].doubleValue
                let lng = locationJson["lng"].doubleValue
                let location = Location(latitude: lat, longitude: lng)
                var reviews: [Review] = []
                for element in item["reviews"].arrayValue {
                    let authorName = element["author_name"].stringValue
                    let rating = element["rating"].doubleValue
                    let text = element["text"].stringValue
                    let relativeTimeDescription = element["relative_time_description"].stringValue
                    let review = Review(authorName: authorName, rating: rating, text: text, relativeTimeDescription: relativeTimeDescription)
                    reviews.append(review)
                }
                let photos = item["photos"]
                var photoReference: String?
                if photos.count > 0 {
                    photoReference = photos[0]["photo_reference"].stringValue
                }
                
                var imageUrlString: String?
                if let photoReference = photoReference {
                    imageUrlString = baseUrl + "photo?maxwidth=1000&photoreference=\(photoReference)&key=\(GooglePlacesAPIKey)"
                }
                
                let place = Place(placeId: placeId, name: name, rating: rating, types: types, openNow: openNow, location: location, reviews: reviews, imageUrlString: imageUrlString)
                return completionHandler(place, nil)
                    
            } catch {
                print("Couldn't parse JSON")
                return completionHandler(nil, .CannotFetch(ErrorMessage.PlaceDetails.Description()))
            }
            
        }
        task.resume()
        
    }
    
    func fetchPlace(placeId: String, completionHandler: @escaping PlacesStoreFetchPlaceCompletionHandler){}
    
    func fetchPlace(placeId: String, completionHandler: @escaping (() throws -> Place?) -> Void){}
    
}
