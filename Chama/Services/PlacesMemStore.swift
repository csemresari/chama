//
//  PlacesMemStore.swift
//  Chama
//
//  Created by swan on 8.05.2020.
//  Copyright © 2020 csemresari. All rights reserved.
//

import Foundation
import SwiftyJSON

class PlacesMemStore: PlacesStoreProtocol {
    
    var places: [Place] {
        do {
            var places: [Place] = []
            guard let path = Bundle(for: type(of:self)).path(forResource: "NearbySearchResponse", ofType: "json") else {
                throw PlacesStoreError.CannotFetch("Cannot fetch places")}
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
            let json = try JSON(data: data)
            
            for item in json["results"].arrayValue {
                
                let placeId = item["place_id"].stringValue
                let name = item["name"].stringValue
                let rating = item["rating"].doubleValue
                let types = item["types"].arrayValue.map{ PlaceType(rawValue: $0.stringValue) ?? .other}
                let openNow = item["opening_hours"]["open_now"].boolValue
                let locationJson = item["geometry"]["location"]
                let lat = locationJson["lat"].doubleValue
                let lng = locationJson["lng"].doubleValue
                let location = Location(latitude: lat, longitude: lng)
                
                let place = Place(placeId: placeId, name: name, rating: rating, types: types, openNow: openNow, location: location)
                places.append(place)
            }
            print("Printing places...")
            print(places)
            return places
        } catch {
            print("Cannot fetch places..")
            return []
        }
    }
    
    var place: Place? {
        do {
            guard let path = Bundle(for: type(of:self)).path(forResource: "PlaceDetailsResponse", ofType: "json") else {
                throw PlacesStoreError.CannotFetch("Cannot fetch place details")}
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
            let json = try JSON(data: data)
            
            let item = json["result"]
            
            let placeId = item["place_id"].stringValue
            let name = item["name"].stringValue
            let rating = item["rating"].doubleValue
            let types = item["types"].arrayValue.map{ PlaceType(rawValue: $0.stringValue) ?? .other}
            let openNow = item["opening_hours"]["open_now"].boolValue
            let locationJson = item["geometry"]["location"]
            let lat = locationJson["lat"].doubleValue
            let lng = locationJson["lng"].doubleValue
            let location = Location(latitude: lat, longitude: lng)
            var reviews: [Review] = []
            for element in item["reviews"].arrayValue {
                let authorName = element["author_name"].stringValue
                let rating = element["rating"].doubleValue
                let text = element["text"].stringValue
                let relativeTimeDescription = element["relative_time_description"].stringValue
                let review = Review(authorName: authorName, rating: rating, text: text, relativeTimeDescription: relativeTimeDescription)
                reviews.append(review)
            }
            let photos = item["photos"]
            var photoReference: String?
            if photos.count > 0 {
                photoReference = photos[0]["photo_reference"].stringValue
            }
            
            var imageUrlString: String?
            if let photoReference = photoReference {
                imageUrlString = baseUrl + "photo?maxwidth=1000&photoreference=\(photoReference)&key=\(GooglePlacesAPIKey)"
            }
            
            return Place(placeId: placeId, name: name, rating: rating, types: types, openNow: openNow, location: location, reviews: reviews, imageUrlString: imageUrlString)
        } catch {
            print("Cannot fetch place details")
            return nil
        }
    }
    
    // MARK: Fetch Places
    
    func fetchPlaces(placeType: PlaceType, completionHandler: @escaping ([Place], PlacesStoreError?) -> Void) {
        completionHandler(places, nil)
    }
    
    func fetchPlaces(placeType: PlaceType, completionHandler: @escaping PlacesStoreFetchPlacesCompletionHandler) {
        completionHandler(PlacesStoreResult.Success(result: self.places))
    }
    
    func fetchPlaces(placeType: PlaceType, completionHandler: @escaping (() throws -> [Place]) -> Void) {
        completionHandler{return places}
    }
    
    
    // MARK: Fetch Place Details
    
    func fetchPlace(placeId: String, completionHandler: @escaping (Place?, PlacesStoreError?) -> Void) {
        
        if let place = place {
            completionHandler(place, nil)
        } else {
            completionHandler (nil, PlacesStoreError.CannotFetch("Cannot fetch place with id \(placeId)"))
        }
    }
    
    func fetchPlace(placeId: String, completionHandler: @escaping PlacesStoreFetchPlaceCompletionHandler) {
        
        if let place = place {
          completionHandler(PlacesStoreResult.Success(result: place))
        } else {
          completionHandler(PlacesStoreResult.Failure(error: PlacesStoreError.CannotFetch("Cannot fetch place with id \(placeId)")))
        }
    }
    
    func fetchPlace(placeId: String, completionHandler: @escaping (() throws -> Place?) -> Void) {
        completionHandler{return place}
        
        if let place = place {
            completionHandler { return place }
        } else {
            completionHandler { throw PlacesStoreError.CannotFetch("Cannot fetch place with id \(placeId)") }
        }
    }
    
    
}
