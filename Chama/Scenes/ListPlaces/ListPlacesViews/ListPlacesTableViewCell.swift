//
//  ListPlacesTableViewCell.swift
//  Chama
//
//  Created by swan on 10.05.2020.
//  Copyright © 2020 csemresari. All rights reserved.
//

import Foundation
import UIKit

class ListPlacesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelRating: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelIsOpen: UILabel!
}
