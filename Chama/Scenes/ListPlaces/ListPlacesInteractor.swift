//
//  ListPlacesInteractor.swift
//  Chama
//
//  Created by swan on 8.05.2020.
//  Copyright © 2020 csemresari. All rights reserved.
//

import CoreLocation

protocol ListPlacesBusinessLogic {
    func fetchPlaces(request: ListPlaces.FetchPlaces.Request)
    func retrieveSortedPlaces(with option: SortOption)
}

protocol ListPlacesDataStore {
    var places: [Place]? {get}
}

class ListPlacesInteractor: ListPlacesBusinessLogic, ListPlacesDataStore {
   
    internal var places: [Place]?
    internal var presenter: ListPlacesPresentationLogic?
    private var placesWorker = PlacesWorker(placesStore: PlacesAPI())
//      private var placesWorker = PlacesWorker(placesStore: PlacesMemStore())
    private var placeIdSet: Set<String>!
    private var hasFetchedBars: Bool!
    private var hasFetchedCafes: Bool!
    private var hasFetchedRestaurants: Bool!
    
    // MARK: - Fetch Places
    
    internal func fetchPlaces(request: ListPlaces.FetchPlaces.Request) {
        
        // Reset
        placeIdSet = []
        places = []
        hasFetchedBars = false
        hasFetchedCafes = false
        hasFetchedRestaurants = false
        
        // Fetch Bars
        placesWorker.fetchPlaces(placeType: .bar) { (places, error) in
            if let error = error {
                self.presenter?.presentFetchPlacesError(error: error)
            }else{
                self.populatePlaces(places: places)
                self.hasFetchedBars = true
                self.presentFetchedPlacesIfAllTypesAreReady()
            }
        }
        
        // Fetch Cafes
        placesWorker.fetchPlaces(placeType: .cafe) { (places, error) in
            if let error = error {
                self.presenter?.presentFetchPlacesError(error: error)
            }else{
                self.populatePlaces(places: places)
                self.hasFetchedCafes = true
                self.presentFetchedPlacesIfAllTypesAreReady()
            }
        }
        
        // Fetch Restaurants
        placesWorker.fetchPlaces(placeType: .restaurant) { (places, error) in
            if let error = error {
                self.presenter?.presentFetchPlacesError(error: error)
            }else{
                self.populatePlaces(places: places)
                self.hasFetchedRestaurants = true
                self.presentFetchedPlacesIfAllTypesAreReady()
            }
        }
    }
    
    internal func retrieveSortedPlaces(with option: SortOption) {
        presenter?.retrieveSortedPlaces(places: places!, with: option)
    }
    
}

// MARK: Utility

extension ListPlacesInteractor {
    
    /// Populates places array while ensuring its items to be distinct
    private func populatePlaces(places: [Place]) {
        for place in places {
            let placeId = place.placeId
            if !placeIdSet.contains(placeId){
                self.places?.append(place)
                placeIdSet.insert(placeId)
            }
        }
    }
    
    /// If fetch requests for all of three types have succeeded, proceed
    private func presentFetchedPlacesIfAllTypesAreReady() {
        if hasFetchedBars && hasFetchedCafes && hasFetchedRestaurants {
            if let places = self.places, places.count > 0 {
                let response = ListPlaces.FetchPlaces.Response(places: places)
                self.presenter?.presentFetchedPlaces(response: response)
            }else{
                print("No places returned")
                self.presenter?.presentFetchPlacesError(error: .CannotFetch(ErrorMessage.Places.Description()))
            }
        }
    }
}

