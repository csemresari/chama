//
//  ListPlacesViewController.swift
//  Chama
//
//  Created by swan on 8.05.2020.
//  Copyright © 2020 csemresari. All rights reserved.
//

import UIKit
import CoreLocation
import NVActivityIndicatorView

protocol ListPlacesDisplayLogic: class {
    func displayFetcedPlaces(viewModel: ListPlaces.FetchPlaces.ViewModel)
    func displaySortedPlaces(viewModel: ListPlaces.FetchPlaces.ViewModel)
    func showFetchPlacesError(error: PlacesStoreError)
}

var currentLocation: CLLocation?
let indicatorType = NVActivityIndicatorType.ballPulseSync
let indicatorDelta: CGFloat = 15

// For ListPlacesViewController the UI part was designed using the Interface Builder.

class ListPlacesViewController: UIViewController, ListPlacesDisplayLogic {

    @IBOutlet weak var barButtonSort: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sortTableView: UITableView!
    @IBOutlet weak var sortTableViewTopConstraint: NSLayoutConstraint!
    
    var interactor: ListPlacesBusinessLogic?
    var router: (NSObjectProtocol & ListPlacesRoutingLogic & ListPlacesDataPassing)?
    var locationManager = CLLocationManager()
    var displayedPlaces: [ListPlaces.FetchPlaces.ViewModel.DisplayedPlace] = []
    var selectedOption = SortOption.rating
    let sortOptions: [SortOption] = [.name, .isOpen, .rating, .distance]
    var hasRequestedPlaces = false
    var activityIndicatorView: NVActivityIndicatorView!
    var sortTableViewRowHeight: CGFloat = 60
    var contentTableViewRowHeight: CGFloat = 65
    
    // MARK: - Initialization
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        construct()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        construct()
    }
    private func construct() {
        let viewController = self
        let interactor = ListPlacesInteractor()
        let presenter = ListPlacesPresenter()
        let router = ListPlacesRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        setupTableViews()
        startLocationServices()
        setupActivityindicator()
        navigationItem.title = "Nearby Places"
    }
    
    private func setupTableViews() {
        // Places
        tableView.register(UINib(nibName: "ListPlacesTableViewCell", bundle: nil), forCellReuseIdentifier: "ListPlacesTableViewCell")
        tableView.tableFooterView = UIView() // Remove the separator lines below the content
        
        // Sort Options
        sortTableView.backgroundColor = .darkGray
        sortTableView.isScrollEnabled = false
        sortTableView.separatorStyle = .none
    }
    
    private func startLocationServices(){
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.delegate = self
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
    }
    
    private func setupActivityindicator() {
        activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: view.center.x - indicatorDelta, y: view.center.y - indicatorDelta, width: 2 * indicatorDelta, height: 2 * indicatorDelta), type: indicatorType, color: view.tintColor)
        view.addSubview(activityIndicatorView)
    }
    
    // MARK: - Content Actions
    
    // MARK: A. Network
    private func fetchPlaces() {
        showActivityIndicator()
        interactor?.fetchPlaces(request: ListPlaces.FetchPlaces.Request())
    }
    
    internal func displayFetcedPlaces(viewModel: ListPlaces.FetchPlaces.ViewModel) {
        hideActivityIndicator()
        displayedPlaces = viewModel.displayedPlaces
        DispatchQueue.main.async { self.tableView.reloadData() }
    }
    
    internal func showFetchPlacesError( error: PlacesStoreError){
        hideActivityIndicator()
        let alert = UIAlertController(title: "Error!", message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { _ in
            self.fetchPlaces()
        }))
        alert.addAction(UIAlertAction(title: "Quit App", style: .cancel, handler: { _ in
            exit(0)
        }))
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: B. Local
    private func retrieveSortedPlaces(option: SortOption) {
        interactor?.retrieveSortedPlaces(with: option)
    }
    
    internal func displaySortedPlaces(viewModel: ListPlaces.FetchPlaces.ViewModel) {
        displayedPlaces = viewModel.displayedPlaces
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        }
    }
    
    // MARK: - UI Actions
    
    @IBAction func sortButtonPressed(_ sender: UIBarButtonItem) {
        presentSortMenuWithAnimation()
    }
}

// MARK: - Table View Delegates

extension ListPlacesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch tableView {
        case sortTableView:
            return sortTableViewRowHeight
        default:
            return contentTableViewRowHeight
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case sortTableView:
            return sortOptions.count
        default:
            return displayedPlaces.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView {
        case sortTableView:
            let cell = UITableViewCell()
            let option = sortOptions[indexPath.row]
            cell.backgroundColor = .darkGray
            cell.textLabel?.text = option.toString() // Capitalizes inside
            cell.textLabel?.textColor = .white
            cell.tintColor = .white
            cell.selectionStyle = .none
            cell.accessoryType = selectedOption == option ? .checkmark : .none // Initial selected option is rating
            return cell
        default:
            let displayedPlace = displayedPlaces[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "ListPlacesTableViewCell", for: indexPath) as! ListPlacesTableViewCell
            cell.selectionStyle = .none
            cell.labelName.text = displayedPlace.name
            cell.labelRating.text = displayedPlace.rating
            cell.labelIsOpen.text = displayedPlace.openNow
            cell.labelDistance.text = displayedPlace.distance
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView {
        case sortTableView:
            let selectedRow = indexPath.row
            let option = sortOptions[selectedRow]
            selectedOption = option // Store currently selected option
            dismissSortMenuWithAnimation() {
                self.interactor?.retrieveSortedPlaces(with: option) // Call content operation when dismiss animation ends
            }
        default:
            router?.routeToShowPlace(segue: nil)
        }
    }
}

// MARK: - Location Delegates

extension ListPlacesViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .denied:
            // Force user to give location permission. As an alternative approach, user's IP address could automatically be used by/in Places API calls however that approach was not preferred at this time and location info was explicitly asked.
            let alert = UIAlertController(title: "Required!", message: "Location permission is required. Plese give permission from the next screen and restart the app.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!) // Open Settings page on the phone
                sleep(2)
                exit(0)
            }))
            self.present(alert, animated: true, completion: nil)
            break
        default:
            // .authorizedAlways, .authorizedWhenInUse
            print("Location permission was granted. Updating locations..")
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last! as CLLocation
        currentLocation = location // Store current location
        print("User location: \nLatitude is:", location.coordinate.latitude, "\nLongitude is:", location.coordinate.longitude)
        
        if !hasRequestedPlaces{
            fetchPlaces()
            hasRequestedPlaces = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        print("ERROR: Location info could not be retrieved:", error.localizedDescription)
    }
}

// MARK: - Utility

extension ListPlacesViewController {
    
    private func showActivityIndicator() {
        DispatchQueue.main.async {
            UIApplication.shared.beginIgnoringInteractionEvents()
            self.activityIndicatorView.startAnimating()
        }
    }
    
    private func hideActivityIndicator(){
        DispatchQueue.main.async {
            UIApplication.shared.endIgnoringInteractionEvents()
            self.activityIndicatorView.stopAnimating()
        }
    }
    
    private func presentSortMenuWithAnimation() {
        sortTableView.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            self.sortTableViewTopConstraint.constant = -self.tableView.frame.height
            self.view.layoutSubviews()
        })
    }
    
    private func dismissSortMenuWithAnimation(completionHandler: (() -> Void)? = nil) {
           
        UIView.animate(withDuration: 0.3, animations: {
            self.sortTableViewTopConstraint.constant = 0
            self.view.layoutSubviews()
        }, completion: { _ in
            self.sortTableView.reloadData()
            self.sortTableView.isHidden = true
            completionHandler?()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

enum SortOption: String {
    case name
    case isOpen
    case rating
    case distance
    
    func toString() -> String {
        switch self {
        case .isOpen:
            return "Open/Close"
        default:
            return self.rawValue.capitalized
        }
    }
}
