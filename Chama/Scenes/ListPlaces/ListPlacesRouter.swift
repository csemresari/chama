//
//  ListPlacesRouter.swift
//  Chama
//
//  Created by swan on 8.05.2020.
//  Copyright © 2020 csemresari. All rights reserved.
//

import UIKit

protocol ListPlacesRoutingLogic {
    func routeToShowPlace(segue: UIStoryboardSegue?)
}

protocol ListPlacesDataPassing {
    var dataStore: ListPlacesDataStore? {get}
}

class ListPlacesRouter: NSObject,ListPlacesRoutingLogic, ListPlacesDataPassing {
    
    internal weak var viewController: ListPlacesViewController?
    internal var dataStore: ListPlacesDataStore?
    
    internal func routeToShowPlace(segue: UIStoryboardSegue?) {
        
        if let segue = segue {
            let destinationVC = segue.destination as! ShowPlaceViewController
            var destinationDS = destinationVC.router!.dataStore!
            passDataToShowPlace(source: dataStore!, destination: &destinationDS)
        }else{
            let destinationVC = viewController?.storyboard?.instantiateViewController(withIdentifier: "ShowPlaceViewController") as! ShowPlaceViewController
            var destinationDS = destinationVC.router!.dataStore!
            passDataToShowPlace(source: dataStore!, destination: &destinationDS)
            navigateToShowOrder(source: viewController!, destination: destinationVC)
        }
    }
    
    private func navigateToShowOrder(source: ListPlacesViewController, destination: ShowPlaceViewController) {
        source.show(destination, sender: nil)
    }
    
    private func passDataToShowPlace(source: ListPlacesDataStore, destination: inout ShowPlaceDataStore) {
        if let viewController = viewController,
            let selectedRow = viewController.tableView.indexPathForSelectedRow?.row {
            let placeId = viewController.displayedPlaces[selectedRow].placeId
            destination.place = source.places!.filter{ $0.placeId == placeId }[0]
        }
    }
}
