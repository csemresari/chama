//
//  ListPlacesPresenter.swift
//  Chama
//
//  Created by swan on 8.05.2020.
//  Copyright © 2020 csemresari. All rights reserved.
//

import CoreLocation

protocol ListPlacesPresentationLogic: class {
    func presentFetchedPlaces(response: ListPlaces.FetchPlaces.Response)
    func retrieveSortedPlaces(places: [Place], with option: SortOption)
    func presentFetchPlacesError(error: PlacesStoreError)
}

class ListPlacesPresenter: ListPlacesPresentationLogic {
    
    internal weak var viewController: ListPlacesDisplayLogic?
    
    // MARK: - Fetch Places
    internal func presentFetchedPlaces(response: ListPlaces.FetchPlaces.Response) {
        
        // Sort the list by rating initially
        let sortedPlaces = sort(places: response.places, with: .rating)
        
        // Create displayed places
        let displayedPlaces = getDisplayedPlaces(sortedPlaces: sortedPlaces)
        
        // Pass via view model
        let viewModel = ListPlaces.FetchPlaces.ViewModel(displayedPlaces: displayedPlaces)
        viewController?.displayFetcedPlaces(viewModel: viewModel)
    }
    
    // MARK: - Retrieve Sorted Places
    internal func retrieveSortedPlaces(places: [Place], with option: SortOption) {
        
        let sortedPlaces = sort(places: places, with: option)
        let displayedPlaces = getDisplayedPlaces(sortedPlaces: sortedPlaces)
        let viewModel = ListPlaces.FetchPlaces.ViewModel(displayedPlaces: displayedPlaces)
        viewController?.displaySortedPlaces(viewModel: viewModel)
    }
    
    // MARK: - Pass Error Info
    internal func presentFetchPlacesError(error: PlacesStoreError) {
        viewController?.showFetchPlacesError(error: error)
    }
}


// MARK: - Utility
extension ListPlacesPresenter {
    
    /// Generates places with fields to be displayed
    private func getDisplayedPlaces(sortedPlaces: [Place]) -> [ListPlaces.FetchPlaces.ViewModel.DisplayedPlace] {
        
        var displayedPlaces: [ListPlaces.FetchPlaces.ViewModel.DisplayedPlace] = []
        
        for place in sortedPlaces {
            
            // Manipulate data and store it in 'displayedPlaces' without mutating permanent data received from Places API
            let displayedPlace = ListPlaces.FetchPlaces.ViewModel.DisplayedPlace(
                placeId: place.placeId,
                name: place.name,
                rating: "★ " + place.rating.formatted(withFractionDigitsOf: 1) + " | " + getTypeString(types: place.types),
                openNow: place.openNow ? "Open" : "Closed",
                distance: getDistanceToCurrentLocation(location: place.location).formatted(withFractionDigitsOf: 1) + " km",
                types: place.types
            )
            displayedPlaces.append(displayedPlace)
        }
        
        return displayedPlaces
    }
    
    /// Sort  places according to the selected sort option
    private func sort(places: [Place], with option: SortOption) -> [Place] {
        
        switch option {
        case .name:
            return places.sorted(by:{ $0.name < $1.name })
        case .isOpen:
            return places.filter{ $0.openNow } + places.filter{ !$0.openNow }
        case .rating:
            return places.sorted(by:{ $0.rating > $1.rating })
        default:
            return places.sorted(by:{ getDistanceToCurrentLocation(location: $0.location) < getDistanceToCurrentLocation(location: $1.location) })
        }
    }
    
    /// Calculate the distance between a place and the user's location as km
    private func getDistanceToCurrentLocation( location: Location) -> Double {
        
        return (currentLocation?.distance(from: CLLocation(latitude: location.latitude, longitude: location.longitude)) ?? 0.0) / 1000
    }
    
    private func getTypeString(types: [PlaceType]) -> String {
        
        if types.contains(.bar) {
            return PlaceType.bar.toString()
        }else if types.contains(.cafe) {
            return PlaceType.cafe.toString()
        }else if types.contains(.restaurant) {
            return PlaceType.restaurant.toString()
        }
        return ""
    }
    
}
