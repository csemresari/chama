//
//  ListPlacesModels.swift
//  Chama
//
//  Created by swan on 8.05.2020.
//  Copyright © 2020 csemresari. All rights reserved.
//


enum ListPlaces {
    
    enum FetchPlaces {
        
        struct Request {
        }
        
        struct Response {
            var places: [Place]
        }
        
        struct ViewModel {
            
            struct DisplayedPlace {
                var placeId: String
                var name: String
                var rating: String
                var openNow: String
                var distance: String
                var types: [PlaceType]
            }
            
            var displayedPlaces: [DisplayedPlace]
        }
    }
}
