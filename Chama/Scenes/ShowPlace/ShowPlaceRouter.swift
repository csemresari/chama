//
//  ShowPlaceRouter.swift
//  Chama
//
//  Created by swan on 8.05.2020.
//  Copyright © 2020 csemresari. All rights reserved.
//

import Foundation

protocol ShowPlaceRoutingLogic {
}

protocol ShowPlaceDataPassing {
    var dataStore: ShowPlaceDataStore? {get}
}

class ShowPlaceRouter: NSObject, ShowPlaceRoutingLogic, ShowPlaceDataPassing {
    
    weak var viewController: ShowPlaceViewController?
    var dataStore: ShowPlaceDataStore?
    
}
