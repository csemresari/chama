//
//  ShowPlaceViewController.swift
//  Chama
//
//  Created by swan on 8.05.2020.
//  Copyright © 2020 csemresari. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import NVActivityIndicatorView

protocol ShowPlaceDisplayLogic: class {
    func displayPlace(viewModel: ShowPlace.GetPlace.ViewModel)
    func showGetPlaceError(error: PlacesStoreError)
}

// For ShowPlaceViewController the UI part was designed using PureLayout library.

class ShowPlaceViewController: UIViewController, ShowPlaceDisplayLogic {
    
    var interactor: ShowPlaceBusinessLogic?
    var router: (NSObject & ShowPlaceRoutingLogic & ShowPlaceDataPassing)?
    var displayedPlace: ShowPlace.GetPlace.ViewModel.DisplayedPlace?
    
    var contentView: UIView!
    var scrollView: UIScrollView!
    var imageView: UIImageView!
    var infoView: UIView!
    var labelName: UILabel!
    var labelRating: UILabel!
    var labelIsOpen: UILabel!
    var tableView: UITableView!
    var tableViewRowHeight: CGFloat = 170
    var activityIndicatorView: NVActivityIndicatorView!
    
    // MARK: - Initialization
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        construct()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        construct()
    }
    
    private func construct() {
        let viewController = self
        let interactor = ShowPlaceInteractor()
        let presenter = ShowPlacePresenter()
        let router = ShowPlaceRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        getPlace()
    }
    
    private func setup() {
        setupUI()
        setupActivityindicator()
        navigationItem.title = "Place Detail"
    }
    
    private func setupUI() {
        
        // Content View
        contentView = UIView(forAutoLayout: ())
        view.addSubview(contentView)
        contentView.autoPinEdge(toSuperviewSafeArea: .top)
        contentView.autoPinEdge(toSuperviewSafeArea: .leading)
        contentView.autoPinEdge(toSuperviewSafeArea: .bottom)
        contentView.autoPinEdge(toSuperviewSafeArea: .trailing)
        
        // Scroll View
        self.scrollView = UIScrollView(forAutoLayout: ())
        contentView.addSubview(scrollView)
        scrollView.autoPinEdge(.top, to: .top, of: contentView)
        scrollView.autoPinEdge(.leading, to: .leading, of: contentView)
        scrollView.autoPinEdge(.bottom, to: .bottom, of: contentView)
        scrollView.autoPinEdge(.trailing, to: .trailing, of: contentView)
        scrollView.bounces = false
        
        // Image View
        imageView = UIImageView(forAutoLayout: ())
        scrollView.addSubview(imageView)
        imageView.backgroundColor = .darkGray
        imageView.contentMode = .scaleAspectFit
        imageView.autoSetDimension(.width, toSize: view.frame.width)
        imageView.autoPinEdge(.top, to: .top, of: scrollView)
        imageView.autoPinEdge(.leading, to: .leading, of: scrollView)
        imageView.autoPinEdge(.trailing, to: .trailing, of: scrollView)
        imageView.autoSetDimension(.height, toSize: view.frame.width)
        
        // Info View
        infoView = UIView(forAutoLayout: ())
        scrollView.addSubview(infoView)
        infoView.backgroundColor = .darkGray
        infoView.autoPinEdge(.top, to: .bottom, of: imageView)
        infoView.autoPinEdge(.leading, to: .leading, of: scrollView)
        infoView.autoPinEdge(.trailing, to: .trailing, of: scrollView)
        infoView.autoSetDimension(.height, toSize: 145)
        
        // Label Name
        labelName = UILabel()
        infoView.addSubview(labelName)
        labelName.textAlignment = .center
        labelName.contentMode = .center
        labelName.textColor = .white
        labelName.numberOfLines = 2
        labelName.autoSetDimension(.height, toSize: 45)
        labelName.autoSetDimension(.width, toSize: view.frame.width - 20)
        labelName.autoConstrainAttribute(.top, to: .top, of: infoView, withOffset: 20)
        labelName.autoAlignAxis(toSuperviewAxis: .vertical)
        
        // Label Rating
        labelRating = UILabel(forAutoLayout: ())
        infoView.addSubview(labelRating)
        labelRating.textColor = .white
        labelRating.textAlignment = .center
        labelRating.autoSetDimension(.height, toSize: 20)
        labelRating.autoAlignAxis(toSuperviewAxis: .vertical)
        labelRating.autoConstrainAttribute(.top, to: .bottom, of: labelName, withOffset: 10)
        
        // Label Is Open/Closed
        labelIsOpen = UILabel(forAutoLayout: ())
        infoView.addSubview(labelIsOpen)
        labelIsOpen.textColor = .white
        labelIsOpen.textAlignment = .center
        labelIsOpen.autoSetDimension(.height, toSize: 20)
        labelIsOpen.autoAlignAxis(toSuperviewAxis: .vertical)
        labelIsOpen.autoConstrainAttribute(.bottom, to: .bottom, of: infoView, withOffset: -20)
        
        // Table View
        tableView = UITableView(forAutoLayout: ())
        tableView.dataSource = self
        tableView.delegate = self
        scrollView.addSubview(tableView)
        tableView.isScrollEnabled = false
        tableView.autoPinEdge(.top, to: .bottom, of: infoView)
        tableView.autoPinEdge(.leading, to: .leading, of: scrollView)
        tableView.autoPinEdge(.trailing, to: .trailing, of: scrollView)
        tableView.autoPinEdge(.bottom, to: .bottom, of: scrollView)
        tableView.register(UINib(nibName: "ShowPlaceTableViewCell", bundle: nil), forCellReuseIdentifier: "ShowPlaceTableViewCell")
    }
    
    private func setupActivityindicator() {
        activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: view.center.x - indicatorDelta, y: view.center.y - indicatorDelta, width: 2 * indicatorDelta, height: 2 * indicatorDelta), type: indicatorType, color: view.tintColor)
        view.addSubview(activityIndicatorView)
    }
    
    // MARK: - Content Actions
    
    // MARK: A. Network
    private func getPlace() {
        
        showActivityIndicator()
        let request = ShowPlace.GetPlace.Request(placeId: router!.dataStore!.place!.placeId)
        interactor?.getPlace(request: request)
    }
    
    internal func displayPlace(viewModel: ShowPlace.GetPlace.ViewModel) {
        
        hideActivityIndicator()
        displayedPlace = viewModel.displayedPlace
        DispatchQueue.main.async {
            self.fillContent()
            self.tableView.reloadData()
        }
    }
    
    internal func showGetPlaceError( error: PlacesStoreError){
        hideActivityIndicator()
        let alert = UIAlertController(title: "Error!", message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { _ in
            self.getPlace()
        }))
        alert.addAction(UIAlertAction(title: "Go Back To List", style: .cancel, handler: { _ in
            self.navigationController?.popToRootViewController(animated: true)
        }))
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: B. Local
    private func fillContent() {
        if let displayedPlace = displayedPlace {
            tableView.autoSetDimension(.height, toSize: tableViewRowHeight * CGFloat(displayedPlace.displayedReviews.count))
            labelName.text = displayedPlace.name
            labelRating.text = displayedPlace.rating
            labelIsOpen.text = displayedPlace.openNow
            let placeholderImage = UIImage(named: "placeholder")
            if let imageUrlString = displayedPlace.imageUrlString,
                let imageUrl = URL(string: imageUrlString) {
                imageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
                imageView.sd_setImage(with: imageUrl, placeholderImage: placeholderImage)
            }else{
                imageView.image = placeholderImage
            }
        }
    }
}

// MARK: - Table View Delegates

extension ShowPlaceViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return tableViewRowHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayedPlace?.displayedReviews.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShowPlaceTableViewCell", for: indexPath) as! ShowPlaceTableViewCell
        if let displayedReview = displayedPlace?.displayedReviews[indexPath.row] {
            cell.labelAuthorName.text = displayedReview.authorName
            cell.labelRating.text = displayedReview.rating
            cell.textViewText.text = displayedReview.text
            cell.labelRelativeTimeDescription.text = displayedReview.relativeTimeDescription
            cell.selectionStyle = .none
        }
        return cell
            
    }
}

// MARK: - Utility

extension ShowPlaceViewController {
    
    private func showActivityIndicator() {
        DispatchQueue.main.async {
            UIApplication.shared.beginIgnoringInteractionEvents()
            self.activityIndicatorView.startAnimating()
        }
    }
    
    private func hideActivityIndicator(){
        DispatchQueue.main.async {
            UIApplication.shared.endIgnoringInteractionEvents()
            self.activityIndicatorView.stopAnimating()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
