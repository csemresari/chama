//
//  ShowPlaceModels.swift
//  Chama
//
//  Created by swan on 8.05.2020.
//  Copyright © 2020 csemresari. All rights reserved.
//

import UIKit

enum ShowPlace {
    
    enum GetPlace {
        
        struct Request {
            var placeId: String
        }
        
        struct Response {
            var place: Place
        }
        
        struct ViewModel {
            struct DisplayedPlace {
                var name: String
                var rating: String
                var openNow: String
                var imageUrlString: String?
                var displayedReviews: [DisplayedReview]
            }
            
            struct DisplayedReview {
                var authorName: String
                var rating: String
                var text: String
                var relativeTimeDescription: String
            }
            
            var displayedPlace: DisplayedPlace
        }
    }
}
