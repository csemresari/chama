//
//  ShowPlacePresenter.swift
//  Chama
//
//  Created by swan on 8.05.2020.
//  Copyright © 2020 csemresari. All rights reserved.
//

import Foundation
import UIKit

protocol ShowPlacePresentationLogic {
    func presentPlace(response: ShowPlace.GetPlace.Response)
    func presentGetPlaceError(error: PlacesStoreError)
}

class ShowPlacePresenter: ShowPlacePresentationLogic {
    
    weak var viewController: ShowPlaceDisplayLogic?
    
    // MARK: - Place Details
    
    internal func presentPlace(response: ShowPlace.GetPlace.Response) {
        
        let place = response.place
        let displayedPlace = ShowPlace.GetPlace.ViewModel.DisplayedPlace(
            name: place.name,
            rating: "★ " + place.rating.description,
            openNow: place.openNow ? "Open Now" : "Closed Now",
            imageUrlString: place.imageUrlString,
            displayedReviews: (place.reviews ?? []).map{
                ShowPlace.GetPlace.ViewModel.DisplayedReview(
                    authorName: $0.authorName.capitalized,
                    rating: $0.rating.formatted(withFractionDigitsOf: 1),
                    text: $0.text,
                    relativeTimeDescription: $0.relativeTimeDescription
                )
            }
        )
        
        let viewModel = ShowPlace.GetPlace.ViewModel(displayedPlace: displayedPlace)
        viewController?.displayPlace(viewModel: viewModel)
    }
    
    // MARK: - Pass Error Info
    internal func presentGetPlaceError(error: PlacesStoreError) {
        viewController?.showGetPlaceError(error: error)
    }
}
