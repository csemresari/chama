//
//  File.swift
//  Chama
//
//  Created by swan on 11.05.2020.
//  Copyright © 2020 csemresari. All rights reserved.
//

import Foundation

class ShowPlaceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelAuthorName: UILabel!
    @IBOutlet weak var labelRating: UILabel!
    @IBOutlet weak var labelRelativeTimeDescription: UILabel!
    @IBOutlet weak var textViewText: UITextView!
}
