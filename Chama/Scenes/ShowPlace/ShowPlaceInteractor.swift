//
//  ShowPlaceInteractor.swift
//  Chama
//
//  Created by swan on 8.05.2020.
//  Copyright © 2020 csemresari. All rights reserved.
//

import Foundation

protocol ShowPlaceBusinessLogic {
    func getPlace(request: ShowPlace.GetPlace.Request)
}

protocol ShowPlaceDataStore {
    var place: Place! {get set}
}

class ShowPlaceInteractor: ShowPlaceBusinessLogic, ShowPlaceDataStore {
    
    var presenter: ShowPlacePresentationLogic?
//    var placesWorker = PlacesWorker(placesStore: PlacesMemStore())
    var placesWorker = PlacesWorker(placesStore: PlacesAPI())
    var place: Place!
    
    // MARK: Get Place Details
    
    func getPlace(request: ShowPlace.GetPlace.Request) {
        
        placesWorker.fetchPlace(placeId: request.placeId) { (place, error) -> Void in
            
            if let place = place {
                self.place = place
                let response = ShowPlace.GetPlace.Response(place: place)
                self.presenter?.presentPlace(response: response)
            }else{
                print(error!.localizedDescription)
                self.presenter?.presentGetPlaceError(error: error!)
            }
        }
    }
}
