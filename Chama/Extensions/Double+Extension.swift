//
//  Double+Extension.swift
//  Chama
//
//  Created by swan on 11.05.2020.
//  Copyright © 2020 csemresari. All rights reserved.
//

import Foundation

extension Double {
    
    static let oneFractionDigits: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 1
        formatter.maximumFractionDigits = 1
        return formatter
    }()
    
    static let twoFractionDigits: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        return formatter
    }()
    
    func formatted(withFractionDigitsOf fractionDigitsOf: Int) -> String {
        switch fractionDigitsOf {
        case 1:
            return Double.oneFractionDigits.string(for: self) ?? ""
        default:
            // 2
            return Double.twoFractionDigits.string(for: self) ?? ""
        }
    }
}
