//
//  PlacesWorker.swift
//  Chama
//
//  Created by swan on 8.05.2020.
//  Copyright © 2020 csemresari. All rights reserved.
//

import Foundation

class PlacesWorker {
    
    var placesStore: PlacesStoreProtocol
    
    init(placesStore: PlacesStoreProtocol) {
        self.placesStore = placesStore
    }
    
    func fetchPlaces(placeType: PlaceType, completionHandler: @escaping ([Place], PlacesStoreError?) -> Void) {
        placesStore.fetchPlaces(placeType: placeType) { (places, error) -> Void in
            
            completionHandler(places, error)
        }
    }
    
    func fetchPlace(placeId: String, completionHandler: @escaping (Place?, PlacesStoreError?) -> Void) {
        placesStore.fetchPlace(placeId: placeId) { (place, error) -> Void in
            
            completionHandler(place, error)
        }
    }
    
    
}

protocol PlacesStoreProtocol {
    
    func fetchPlaces(placeType: PlaceType, completionHandler: @escaping ([Place], PlacesStoreError?) -> Void)
    func fetchPlaces(placeType: PlaceType, completionHandler: @escaping PlacesStoreFetchPlacesCompletionHandler)
    func fetchPlaces(placeType: PlaceType, completionHandler: @escaping (() throws -> [Place]) -> Void)
    
    func fetchPlace(placeId: String, completionHandler: @escaping (Place?, PlacesStoreError?) -> Void)
    func fetchPlace(placeId: String, completionHandler: @escaping PlacesStoreFetchPlaceCompletionHandler)
    func fetchPlace(placeId: String, completionHandler: @escaping (() throws -> Place?) -> Void)
}

typealias PlacesStoreFetchPlacesCompletionHandler = (PlacesStoreResult<[Place]>) -> Void
typealias PlacesStoreFetchPlaceCompletionHandler = (PlacesStoreResult<Place>) -> Void

enum PlacesStoreResult<T> {
    case Success(result: T)
    case Failure(error: PlacesStoreError)
}

enum PlacesStoreError: Error, Equatable {
    case CannotFetch(String)
}

extension PlacesStoreError: LocalizedError {
    
    public var errorDescription: String? {
        switch self {
        case .CannotFetch(let message):
            return message
        }
    }
}


